# SadarSiaga Rails API

[![license](https://img.shields.io/github/license/mashape/apistatus.svg)]()
[![GitHub release](https://img.shields.io/github/release/phw/peek.svg)](https://github.com/phw/peek/releases)
[![Twitter URL](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)]()
[![Translation Status](https://hosted.weblate.org/widgets/peek/-/svg-badge.svg)](https://hosted.weblate.org/engage/peek/?utm_source=widget)
[![label](https://img.shields.io/github/issues-raw/badges/shields/website.svg)]()  



Sadarsiaga adalah aplikasi notifikasi peringatan potensi bencana alam yang
mengambil data dari BMKG, chating privat desa dan broadcast pengumuman desa.

## Getting Started

This Project creating simple CRUD API for SadarSiaga Application using Ruby on Rails. You
# Deps
- Install Ruby 2.5.1
- Install Rails 5.2.1
- Install heroku CLI
- Install Postgresql
- Postman for testing
- Redis

# Running in local

- Clone this appp :

      git clone .....

- Run by : 

     Rails s

- Open localhost:3000/<endpoint>/


# Running in Heroku
![Peek recording itself](https://github.com/ervinismu/kejarmimpi/blob/master/asset/Peek%202017-09-22%2010-49.gif)
- Download this app :

      git clone gitlab.com:Binar-Bootcamp-Jogja/Batch-9/team-e/backend.git

- Login akun : 

      Heroku login

- Create apps : 

      heroku create -b .....

- Remote apps in Heroku :

       heroku git:remote -a <your-apps-name>

- Create database postgresql : 

      Heroku addons:create heroku-postgresql:hobby-dev

- Push : 

      Git push heroku master

- Open app : 

      Heroku open
