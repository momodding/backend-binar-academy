# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# warga = Warga.new
# warga.name = 'Farid'
# warga.rt_id = 1
# warga.nik = "12345"
# warga.email = "faridalrafi@gmail.com"
# warga.password_digest = "12345"
# warga.save

# user
User.create([
              { id: 1, name: 'Admin', email: 'admin@time.com', password: 'password', role: 'admin', key_type: 'forgot', is_active: true },
              { id: 2, name: 'Desa Client', email: 'desa@time.com', password: 'password', role: 'desa', is_active: true },
              { id: 3, name: 'Admin Kelurahan Binar', email: 'binar@time.com', password: 'password', role: 'desa', is_active: true }
            ])

Desa.create([
              {
                id: 1,
                name: 'Condong Catur',
                province: 'Daerah Istimewa Yogyakarta',
                district: 'Sleman',
                subdistrict: 'Depok',
                user_id: 2
              },
              {
                id: 2,
                name: 'Kelurahan Gorongan',
                province: 'Daerah Istimewa Yogyakarta',
                district: 'Sleman',
                subdistrict: 'Depok',
                user_id: 3
              }
            ])

Dukuh.create([
               { id: 1, name: 'Front End', desa_id: 2 },
               { id: 2, name: 'Back End', desa_id: 2 },
               { id: 3, name: 'Product Manager', desa_id: 2 },
               { id: 4, name: 'QA', desa_id: 2 },
               { id: 5, name: 'UIX', desa_id: 2 },
               { id: 6, name: 'Mancasan Lor', desa_id: 1 },
               { id: 7, name: 'Mancasan Kidul', desa_id: 1 },
               { id: 8, name: 'Perumnas', desa_id: 1 }
             ])

Rw.create([
            { id: 1, name: 'RW 1 - Backend Web', dukuh_id: 2 },
            { id: 2, name: 'RW 2 - Backend App', dukuh_id: 2 },
            { id: 3, name: 'RW 1 - FDesa', dukuh_id: 1 },
            { id: 4, name: 'RW 1 - FAdmin', dukuh_id: 1 },
            { id: 5, name: 'RW 1', dukuh_id: 6 },
            { id: 6, name: 'RW 2', dukuh_id: 6 },
            { id: 7, name: 'RW 3', dukuh_id: 6 },
            { id: 8, name: 'RW 1', dukuh_id: 7 },
            { id: 9, name: 'RW 2', dukuh_id: 7 }
          ])

Rt.create([
            { id: 1, name: 'RT 1 - Dashboard', rw_id: 1 },
            { id: 2, name: 'RT 2 - Dukuh', rw_id: 1 },
            { id: 3, name: 'RT 3 - RW', rw_id: 1 },
            { id: 4, name: 'RT 4 - RT', rw_id: 1 },
            { id: 5, name: 'RT 5 - Warga', rw_id: 1 },
            { id: 6, name: 'RT 1 - Login', rw_id: 2 },
            { id: 7, name: 'RT 2 - Bantuan', rw_id: 2 },
            { id: 8, name: 'RT 3 - Chat', rw_id: 2 },
            { id: 9, name: 'RT 4 - Berita', rw_id: 2 }
          ])

Dukuh.create([
               { id: 1, name: 'Mancasan Lor', desa_id: 1 },
               { id: 2, name: 'Mancasan Kidul', desa_id: 1 },
               { id: 3, name: 'Perumnas', desa_id: 1 },
               { id: 4, name: 'Front End', desa_id: 2 },
               { id: 5, name: 'Back End', desa_id: 2 },
               { id: 6, name: 'Product Manager', desa_id: 2 },
               { id: 7, name: 'QA', desa_id: 2 },
               { id: 8, name: 'UIX', desa_id: 2 }
             ])

Rw.create([
            { id: 1, name: 'RW 1 dukuh mancasan lor', dukuh_id: 1 },
            { id: 2, name: 'RW 2 dukuh mancasan lor', dukuh_id: 1 },
            { id: 3, name: 'RW 1 dukuh mancasan kidul', dukuh_id: 2 },
            { id: 4, name: 'RW 1 dukuh Perumnas', dukuh_id: 3 },
            { id: 5, name: 'RW 1', dukuh_id: 4 },
            { id: 6, name: 'RW 2', dukuh_id: 5 },
            { id: 7, name: 'RW 3', dukuh_id: 6 },
            { id: 8, name: 'RW 1', dukuh_id: 7 },
            { id: 9, name: 'RW 2', dukuh_id: 8 }
          ])

Rt.create([
            { id: 1, name: 'RT 1 - Mlor', rw_id: 1 },
            { id: 2, name: 'RT 2 - Mlor', rw_id: 1 },
            { id: 3, name: 'RT 3 - Mlor rw2', rw_id: 2 },
            { id: 4, name: 'RT 4 - Mlor rw2', rw_id: 2 },
            { id: 5, name: 'RT 1 - Mkidul', rw_id: 3 },
            { id: 6, name: 'RT 1 - Mkidul', rw_id: 3 },
            { id: 7, name: 'RT 2 - Mkidul', rw_id: 3 },
            { id: 8, name: 'RT 1 - Perumnas', rw_id: 4 },
            { id: 9, name: 'RT 2 - perumnas rw1', rw_id: 4 }
          ])

Warga.create([
               {
                 name: 'Warga Kepala Desa',
                 nik: '1234567890',
                 email: 'warga@time.com',
                 password: 'password',
                 phone: '081234567890',
                 is_active: true,
                 role: 'desa',
                 dukuh_id: 1,
                 rw_id: 1,
                 rt_id: 1,
                 desa_id: 1
               }
             ])
