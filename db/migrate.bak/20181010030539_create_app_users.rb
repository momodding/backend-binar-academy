# frozen_string_literal: true

class CreateAppUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :app_users do |t|
      t.text :name
      t.text :email
      t.text :password
      t.string :role
      t.references :warga, foreign_key: true

      t.timestamps
    end
  end
end
