# frozen_string_literal: true

class CreateRws < ActiveRecord::Migration[5.2]
  def change
    create_table :rws do |t|
      t.string :name

      t.timestamps
    end
  end
end
