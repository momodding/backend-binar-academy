# frozen_string_literal: true

class CreateRts < ActiveRecord::Migration[5.2]
  def change
    create_table :rts do |t|
      t.string :name

      t.timestamps
    end
  end
end
