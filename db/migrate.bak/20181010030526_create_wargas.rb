# frozen_string_literal: true

class CreateWargas < ActiveRecord::Migration[5.2]
  def change
    create_table :wargas do |t|
      t.text :name
      t.references :rt, foreign_key: true
      t.references :rw, foreign_key: true
      t.references :desa, foreign_key: true

      t.timestamps
    end
  end
end
