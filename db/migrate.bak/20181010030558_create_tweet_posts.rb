# frozen_string_literal: true

class CreateTweetPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :tweet_posts do |t|
      t.integer :user_tweets_id
      t.integer :tweets_id
      t.text :title
      t.text :description
      t.text :url
      t.text :img_url

      t.timestamps
    end
  end
end
