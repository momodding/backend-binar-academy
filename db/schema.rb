# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_11_06_132309) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "broadcast_posts", force: :cascade do |t|
    t.string "content"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "desa_registers", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "province"
    t.string "district"
    t.string "subdistrict"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "desas", force: :cascade do |t|
    t.string "name"
    t.string "province"
    t.string "district"
    t.string "subdistrict"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dukuhs", force: :cascade do |t|
    t.string "name"
    t.integer "desa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "helps", force: :cascade do |t|
    t.string "title"
    t.integer "desa_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rts", force: :cascade do |t|
    t.string "name"
    t.integer "rw_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rws", force: :cascade do |t|
    t.string "name"
    t.integer "dukuh_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "tweet_posts", force: :cascade do |t|
    t.integer "user_tweets_id"
    t.bigint "tweets_id"
    t.text "title"
    t.text "description"
    t.text "url"
    t.text "img_url"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.string "role"
    t.string "key_secret"
    t.string "key_type"
    t.boolean "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wargas", force: :cascade do |t|
    t.string "name"
    t.string "nik"
    t.string "email"
    t.string "password_digest"
    t.integer "rt_id"
    t.boolean "is_active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "dukuh_id"
    t.integer "rw_id"
    t.string "role"
    t.string "phone"
    t.integer "desa_id"
    t.text "personal_status"
    t.text "alamat"
  end

end
