# frozen_string_literal: true

class CreateDukuhs < ActiveRecord::Migration[5.2]
  def change
    create_table :dukuhs do |t|
      t.string :name
      t.integer :desa_id, foreign_key: true
      t.integer :warga_id

      t.timestamps
    end
  end
end
