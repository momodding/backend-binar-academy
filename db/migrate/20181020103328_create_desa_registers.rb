# frozen_string_literal: true

class CreateDesaRegisters < ActiveRecord::Migration[5.2]
  def change
    create_table :desa_registers do |t|
      t.string :name
      t.string :email
      t.string :province
      t.string :district
      t.string :subdistrict
      t.string :file

      t.timestamps
    end
  end
end
