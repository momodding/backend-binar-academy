# frozen_string_literal: true

class CreateWargas < ActiveRecord::Migration[5.2]
  def change
    create_table :wargas do |t|
      t.string :name
      t.string :nik
      t.string :email
      t.string :password_digest
      t.integer :rt_id, foreign_key: true
      t.boolean :is_active

      t.timestamps
    end
  end
end
