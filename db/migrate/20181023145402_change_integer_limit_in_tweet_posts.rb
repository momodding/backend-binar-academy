# frozen_string_literal: true

class ChangeIntegerLimitInTweetPosts < ActiveRecord::Migration[5.2]
  def change
    change_column :tweet_posts, :tweets_id, :integer, limit: 8
  end
end
