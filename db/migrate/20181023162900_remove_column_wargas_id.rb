# frozen_string_literal: true

class RemoveColumnWargasId < ActiveRecord::Migration[5.2]
  def change
    remove_column :rts, :warga_id, :integer
    remove_column :rws, :warga_id
    remove_column :desas, :warga_id

    remove_column :dukuhs, :warga_id
  end
end
