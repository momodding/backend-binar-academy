# frozen_string_literal: true

class CreateBroadcastPosts < ActiveRecord::Migration[5.2]
  def change
    create_table :broadcast_posts do |t|
      t.string :content
      t.bigint :user_id

      t.timestamps
    end
  end
end
