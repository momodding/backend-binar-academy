# frozen_string_literal: true

class AddDesaIdToWargas < ActiveRecord::Migration[5.2]
  def change
    add_column :wargas, :desa_id, :integer, foreign_key: true
  end
end
