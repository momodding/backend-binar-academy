# frozen_string_literal: true

class CreateHelps < ActiveRecord::Migration[5.2]
  def change
    create_table :helps do |t|
      t.string :title
      t.integer :desa_id

      t.timestamps
    end
  end
end
