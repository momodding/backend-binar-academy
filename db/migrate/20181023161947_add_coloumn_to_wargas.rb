# frozen_string_literal: true

class AddColoumnToWargas < ActiveRecord::Migration[5.2]
  def change
    add_column :wargas, :dukuh_id, :integer, foreign_key: true
    add_column :wargas, :rw_id, :integer, foreign_key: true

    add_column :wargas, :role, :integer
  end
end
