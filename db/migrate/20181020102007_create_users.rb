# frozen_string_literal: true

class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :email
      t.string :password_digest
      t.string :role
      t.string :key_secret
      t.string :key_type
      t.boolean :is_active

      t.timestamps
    end
  end
end
