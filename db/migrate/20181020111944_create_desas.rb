# frozen_string_literal: true

class CreateDesas < ActiveRecord::Migration[5.2]
  def change
    create_table :desas do |t|
      t.string :name
      t.string :province
      t.string :district
      t.string :subdistrict
      t.integer :user_id
      t.integer :warga_id, foreign_key: true

      t.timestamps
    end
  end
end
