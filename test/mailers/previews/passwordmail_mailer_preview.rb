# frozen_string_literal: true

# Preview all emails at http://localhost:3000/rails/mailers/passwordmail_mailer
class PasswordmailMailerPreview < ActionMailer::Preview
end
