# frozen_string_literal: true

# require "test_helper"

# class ResetPasswordMailerTest < ActionMailer::TestCase
#   test "reset password confirmation" do
#     user = FactoryBot.create :user
#     email = ResetPasswordMailer.reset_email(user = user.email).deliver_now

#     assert_not ActionMailer::Base.deliveries.empty?

#     assert_equal ["from@example.com"], email.from
#     assert_equal [user.email], email.to
#     assert_equal "Sadar Siaga Reset Password", email.subject
#   end
# end
