# frozen_string_literal: true

FactoryBot.define do
  factory :rw do
    name { Faker::Number.number(10) }
    dukuh
  end
end
