# frozen_string_literal: true

FactoryBot.define do
  factory :tweet_post do
    user_tweets_id { Faker::Number.number(2) }
    tweets_id { Faker::Number.number(2) }
    title { Faker::Lorem.words }
    description { Faker::Lorem.sentence }
    url { Faker::Internet.url }
    img_url { Faker::Internet.url }
  end
end
