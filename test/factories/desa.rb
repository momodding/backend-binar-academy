# frozen_string_literal: true

FactoryBot.define do
  factory :desa do
    name { Faker::Nation.capital_city }
    province { Faker::Nation.capital_city }
    district { Faker::Nation.capital_city }
    subdistrict { Faker::Nation.capital_city }
    user
  end
end
