# frozen_string_literal: true

FactoryBot.define do
  factory :warga do
    name { Faker::Name.name }
    nik { Faker::Lorem.word }
    email { Faker::Internet.email }
    password { Faker::Lorem.word }
    role { 'warga' }
    rt
    rw
    dukuh
  end
end
