# frozen_string_literal: true

FactoryBot.define do
  factory :desa_register do
    name { Faker::Nation.capital_city }
    email { Faker::Internet.email }
    province { Faker::Nation.capital_city }
    district { Faker::Nation.capital_city }
    subdistrict { Faker::Nation.capital_city }
  end
end
