# frozen_string_literal: true

FactoryBot.define do
  factory :help do
    title { 'MyString' }
    desa_id { 1 }
  end
end
