# frozen_string_literal: true

FactoryBot.define do
  factory :rt do
    name { Faker::Number.number(10) }
    rw
  end
end
