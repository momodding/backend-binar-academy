# frozen_string_literal: true

FactoryBot.define do
  factory :dukuh do
    name { Faker::Nation.capital_city }
    desa
  end
end
