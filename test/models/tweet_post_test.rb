# frozen_string_literal: true

require 'test_helper'

class TweetPostTest < ActiveSupport::TestCase
  test 'should create tweet_post' do
    tweet_post = FactoryBot.create :tweet_post
    assert tweet_post.valid?
  end

  test 'should not create tweet_post where about is nil' do
    tweet_post = FactoryBot.build :tweet_post, title: nil
    assert_not tweet_post.valid?
  end
end
