# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test 'should create user' do
    user = FactoryBot.create :user
    assert user.valid?
  end

  test 'should not create user where about is nil' do
    user = FactoryBot.build :user, name: nil
    assert_not user.valid?
  end
end
