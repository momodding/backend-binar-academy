# frozen_string_literal: true

require 'test_helper'

class BroadcastPostTest < ActiveSupport::TestCase
  test 'should create broadcast' do
    broadcast_post = FactoryBot.create :broadcast_post
    assert broadcast_post.valid?
  end

  test 'should not create broadcast where about is nil' do
    broadcast_post = FactoryBot.build :broadcast_post, user_id: nil
    assert_not broadcast_post.valid?
  end
end
