# frozen_string_literal: true

require 'test_helper'

class DesaRegisterTest < ActiveSupport::TestCase
  test 'should create desa register' do
    desa_register = FactoryBot.create :desa_register
    assert desa_register.valid?
  end

  test 'should not create desa register where about is nil' do
    desa_register = FactoryBot.build :desa_register, name: nil
    assert_not desa_register.valid?
  end
end
