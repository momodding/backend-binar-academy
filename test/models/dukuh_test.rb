# frozen_string_literal: true

require 'test_helper'

class DukuhTest < ActiveSupport::TestCase
  test 'should create dukuh' do
    dukuh = FactoryBot.create :dukuh
    assert dukuh.valid?
  end

  test 'should not create dukuh where about is nil' do
    dukuh = FactoryBot.build :dukuh, name: nil
    assert_not dukuh.valid?
  end
end
