# frozen_string_literal: true

require 'test_helper'

class RtTest < ActiveSupport::TestCase
  test 'should create rt' do
    rt = FactoryBot.create :rt
    assert rt.valid?
  end

  test 'should not create rt where about is nil' do
    rt = FactoryBot.build :rt, name: nil
    assert_not rt.valid?
  end
end
