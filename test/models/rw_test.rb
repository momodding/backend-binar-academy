# frozen_string_literal: true

require 'test_helper'

class RwTest < ActiveSupport::TestCase
  test 'should create rw' do
    rw = FactoryBot.create :rw
    assert rw.valid?
  end

  test 'should not create rw where about is nil' do
    rw = FactoryBot.build :rw, name: nil
    assert_not rw.valid?
  end
end
