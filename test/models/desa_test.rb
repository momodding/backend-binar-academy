# frozen_string_literal: true

require 'test_helper'

class DesaTest < ActiveSupport::TestCase
  test 'should create desa' do
    desa = FactoryBot.create :desa
    assert desa.valid?
  end

  test 'should not create desa where about is nil' do
    desa = FactoryBot.build :desa, name: nil
    assert_not desa.valid?
  end
end
