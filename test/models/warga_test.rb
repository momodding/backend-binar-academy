# frozen_string_literal: true

require 'test_helper'

class WargaTest < ActiveSupport::TestCase
  test 'should create warga' do
    warga = FactoryBot.create :warga
    assert warga.valid?
  end

  test 'should not create warga where about is nil' do
    warga = FactoryBot.build :warga, name: nil
    assert_not warga.valid?
  end
end
