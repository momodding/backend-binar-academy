# frozen_string_literal: true

require 'test_helper'

class DukuhsControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry
    @logged_id = user.id
    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  def setup
    @desa = FactoryBot.create :desa
  end

  test 'should get index' do
    # require "pry"
    # binding.pry
    auth
    FactoryBot.create :desa, user_id: @logged_id
    get web_dukuhs_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get show' do
    auth
    # require "pry"
    # binding.pry
    dukuh = FactoryBot.create :dukuh
    get web_dukuhs_url + "/#{dukuh.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    post web_dukuhs_url, params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    post web_dukuhs_url, params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should get put' do
    auth
    # require "pry"
    # binding.pry
    dukuh = FactoryBot.create :dukuh
    put web_dukuhs_url + "/#{dukuh.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :no_content
  end

  test 'should get delete' do
    auth
    # require "pry"
    # binding.pry
    dukuh = FactoryBot.create :dukuh
    delete web_dukuhs_url + "/#{dukuh.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :no_content
  end

  private

  def params
    desa_id = FactoryBot.create :desa, user_id: @logged_id
    # {
    #   "name": Faker::Name.name,
    #   "desa_id": desa_id.id
    # }.stringify_keys
  end

  def err_params
    desa_id = FactoryBot.create :desa, user_id: @logged_id
    {
      "dukuh":
      {
        "name": Faker::Name.name,
        "desa_id": desa_id.id
      }.stringify_keys
    }.stringify_keys
  end
end
