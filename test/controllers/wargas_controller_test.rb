# frozen_string_literal: true

require 'test_helper'

class WargasControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  def setup
    @user = FactoryBot.create :user
    @desa = FactoryBot.create :desa
    @dukuh = FactoryBot.create :dukuh
    @rw = FactoryBot.create :rw
    @rt = FactoryBot.create :rt
  end

  test 'should get index' do
    auth
    get web_wargas_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get show' do
    auth
    # require "pry"
    # binding.pry
    warga = FactoryBot.create :warga
    get web_wargas_url + "/#{warga.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    post web_wargas_url, params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    post web_wargas_url, params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should get put' do
    auth
    # require "pry"
    # binding.pry
    warga = FactoryBot.create :warga
    put web_wargas_url + "/#{warga.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should get delete' do
    auth
    # require "pry"
    # binding.pry
    warga = FactoryBot.create :warga
    delete web_wargas_url + "/#{warga.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  private

  def params
    {
      "name": Faker::Name.name,
      "nik": Faker::Number.number(10),
      "email": Faker::Internet.email,
      "password": Faker::Internet.password,
      "phone": Faker::Number.number(10),
      "role": 'warga',
      "rt_id": @rt.id,
      "rw_id": @rw.id,
      "dukuh_id": @dukuh.id
    }.stringify_keys
  end

  def err_params
    {
      "warga":
      {
        "name": Faker::Name.name,
        "nik": Faker::Number.number(10),
        "email": Faker::Internet.email,
        "password_digest": Faker::Internet.password,
        "phone": Faker::Number.number(10),
        "role": 'warga',
        "rt_id": @rt.id,
        "rw_id": @rw.id,
        "dukuh_id": @dukuh.id
      }.stringify_keys
    }.stringify_keys
  end
end
