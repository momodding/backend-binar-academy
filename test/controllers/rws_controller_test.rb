# frozen_string_literal: true

require 'test_helper'

class RwsControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  def setup
    @dukuh = FactoryBot.create :dukuh
  end

  test 'should get index' do
    auth
    get web_rws_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get show' do
    auth
    # require "pry"
    # binding.pry
    rw = FactoryBot.create :rw
    get web_rws_url + "/#{rw.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    post web_rws_url, params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    post web_rws_url, params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should get put' do
    auth
    # require "pry"
    # binding.pry
    rw = FactoryBot.create :rw
    put web_rws_url + "/#{rw.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :no_content
  end

  test 'should get delete' do
    auth
    # require "pry"
    # binding.pry
    rw = FactoryBot.create :rw
    delete web_rws_url + "/#{rw.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :no_content
  end

  private

  def params
    {
      "name": Faker::Name.name,
      "dukuh_id": @dukuh.id
    }.stringify_keys
  end

  def err_params
    {
      "rw":
      {
        "name": Faker::Name.name,
        "dukuh_id": @dukuh.id
      }.stringify_keys
    }.stringify_keys
  end
end
