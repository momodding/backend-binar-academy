# frozen_string_literal: true

require 'test_helper'

class RtControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  def setup
    @rw = FactoryBot.create :rw
  end

  test 'should get index' do
    auth
    get web_rt_index_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get show' do
    auth
    # require "pry"
    # binding.pry
    rt = FactoryBot.create :rt
    get web_rt_index_url + "/#{rt.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    post web_rt_index_url, params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    post web_rt_index_url, params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should get put' do
    auth
    # require "pry"
    # binding.pry
    rt = FactoryBot.create :rt
    put web_rt_index_url + "/#{rt.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should get delete' do
    auth
    # require "pry"
    # binding.pry
    rt = FactoryBot.create :rt
    delete web_rt_index_url + "/#{rt.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  private

  def params
    {
      "name": Faker::Name.name,
      "rw_id": @rw.id
    }.stringify_keys
  end

  def err_params
    {
      "rt":
      {
        "name": Faker::Name.name,
        "rw_id": @rw.id
      }.stringify_keys
    }.stringify_keys
  end
end
