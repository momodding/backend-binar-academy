# frozen_string_literal: true

require 'test_helper'

class BroadcastsControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry
    @login_id = user.id

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  # def setup
  #   @user = FactoryBot.create :user
  # end

  # test 'should get index' do
  #   auth
  #   get web_desa_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  test 'should get show' do
    auth
    # require "pry"
    # binding.pry
    broadcast_post = FactoryBot.create :broadcast_post, user_id: @login_id
    get "/web/broadcast/#{@login_id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    broadcast_post = FactoryBot.create :broadcast_post, user_id: @login_id
    post "/web/broadcast/create/#{@login_id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    broadcast_post = FactoryBot.create :broadcast_post, user_id: @login_id
    post "/web/broadcast/create/#{@login_id}", params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  # test 'should get put' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   desa = FactoryBot.create :desa
  #   put web_desa_url + "/#{desa.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :created
  # end

  # test 'should get delete' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   desa = FactoryBot.create :desa
  #   delete web_desa_url + "/#{desa.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  private

  def params
    {
      "content": Faker::Lorem.sentence
    }.stringify_keys
  end

  def err_params
    {
      "broadcast":
      {
        "content": Faker::Lorem.sentence
      }.stringify_keys
    }.stringify_keys
  end
end
