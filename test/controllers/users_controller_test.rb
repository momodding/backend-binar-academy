# frozen_string_literal: true

require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  def setup
    @user = FactoryBot.create :user
  end

  # test 'should get index' do
  #   auth
  #   get web_rws_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  # test 'should get show' do
  #   # auth
  #   # require "pry"
  #   # binding.pry
  #   rw = FactoryBot.create :rw
  #   get web_rws_url + "/#{rw.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  test 'should get post login' do
    # auth
    # require "pry"
    # binding.pry
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    post web_auth_login_url, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
    assert_response :created
  end

  test 'should get post checktoken' do
    auth
    # require "pry"
    # binding.pry
    post web_auth_checktoken_url, params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :ok
  end

  test 'should not get post checktoken' do
    auth
    # require "pry"
    # binding.pry
    post web_auth_checktoken_url, params: nil, headers: { 'Authorization' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NDEwMzk3MjIsInN1YiI6Mn0.9FSOKXEzN3XXMjMAYSlTLnfsge--kp8ReRoDQYI5P-k', 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  test 'should get post' do
    # auth
    # require "pry"
    # binding.pry
    post web_auth_admin_signup_url, params: params, headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    # auth
    # require "pry"
    # binding.pry
    post web_auth_admin_signup_url, params: err_params, headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  # test 'should get post reset password' do
  #   # auth
  #   # require "pry"
  #   # binding.pry
  #   assert_difference "ActionMailer::Base.deliveries.size", +1 do
  #     post web_auth_reset_password_url, params: mail#, headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   end

  #   email = ActionMailer::Base.deliveries.last

  #   assert_equal ["time.binar@gmail.com"], email.from
  #   assert_equal [user.email], email.to
  #   assert_equal "Sadar Siaga Reset Password", email.subject
  #   assert_response :created
  # end

  # test 'set user' do
  #   # auth
  #   # require "pry"
  #   # binding.pry
  #   user = FactoryBot.create :user

  #   assert_equal user, Web::UsersController.send(:reset_password)
  # end

  # test 'should get put' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   rw = FactoryBot.create :rw
  #   put web_rws_url + "/#{rw.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :no_content
  # end

  # test 'should get delete' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   rw = FactoryBot.create :rw
  #   delete web_rws_url + "/#{rw.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :no_content
  # end

  # private

  def params
    {
      name: 'Admin',
      email: 'admin@time.com',
      password: 'password',
      role: 'admin',
      key_type: 'forgot',
      is_active: true
    }.stringify_keys
  end

  def err_params
    {
      "user":
        {
          name: 'Admin',
          email: 'admin@time.com',
          password: 'password',
          role: 'admin',
          key_type: 'forgot',
          is_active: true
        }.stringify_keys
    }.stringify_keys
  end

  def mail
    {
      "email": @user.email
    }
  end
end
