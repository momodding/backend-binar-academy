# frozen_string_literal: true

require 'test_helper'

class DesaRegistersControllerTest < ActionDispatch::IntegrationTest
  def auth
    user = FactoryBot.create :user, email: 'admin@time.com', password: 'admin'
    # require "pry"
    # binding.pry

    post web_auth_login_path, params: "{ \"auth\":{ \"email\":\"#{user.email}\", \"password\":\"#{user.password}\" } }", headers: { 'Content-Type' => 'application/json', 'Accept' => 'application/json' }
  end

  # def setup
  #   @user = FactoryBot.create :user
  # end

  # test 'should get index' do
  #   auth
  #   get web_desa_url, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  # test 'should get show' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   desa = FactoryBot.create :desa
  #   get web_desa_url + "/#{desa.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  test 'should get post' do
    auth
    # require "pry"
    # binding.pry
    post web_auth_signup_url, params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :created
  end

  test 'should not get post' do
    auth
    # require "pry"
    # binding.pry
    post web_auth_signup_url, params: err_params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
    assert_response :unprocessable_entity
  end

  # test 'should get put' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   desa = FactoryBot.create :desa
  #   put web_desa_url + "/#{desa.id}", params: params, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :created
  # end

  # test 'should get delete' do
  #   auth
  #   # require "pry"
  #   # binding.pry
  #   desa = FactoryBot.create :desa
  #   delete web_desa_url + "/#{desa.id}", params: nil, headers: { 'Authorization' => JSON.parse(@response.body)['jwt'], 'Content-Type' => 'application/json', 'Accept' => 'application/json' }, as: :json
  #   assert_response :ok
  # end

  private

  def params
    {
      "name": Faker::Nation.capital_city,
      "email": Faker::Internet.email,
      "province": Faker::Nation.capital_city,
      "district": Faker::Nation.capital_city,
      "subdistrict": Faker::Nation.capital_city
    }.stringify_keys
  end

  def err_params
    {
      "desa":
      {
        "name": Faker::Nation.capital_city,
        "email": Faker::Internet.email,
        "province": Faker::Nation.capital_city,
        "district": Faker::Nation.capital_city,
        "subdistrict": Faker::Nation.capital_city
      }.stringify_keys
    }.stringify_keys
  end
end
