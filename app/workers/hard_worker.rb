# frozen_string_literal: true

class HardWorker
  include Sidekiq::Worker

  def perform(email)
    # Do something
    UserMailer.welcome_email(email).deliver_now
  end
end
