# frozen_string_literal: true
class App::WargasController < AppController
  before_action :set_warga, only: %i[update]

  def check_token
    if current_warga
      render json: {
        status: 'success',
        message: 'Token Valid',
        data: current_warga
      },
      status: :ok
    else
      render json: {
        status: 'error',
        message: 'Token Invalid'
      },
      status: :unprocessable_entity
    end
  end

  def profile
    if current_warga
      @desa = Desa.find(current_warga.desa_id)
      @dukuh = Dukuh.find(current_warga.dukuh_id)
      render json: {
        status: 'success',
        message: 'Token Valid',
        data: current_warga,
        desa: @desa.name,
        dukuh: @dukuh.name
      },
      status: :ok
    else
      render json: {
        status: 'error',
        message: 'Token Invalid'
      },
      status: :unprocessable_entity
    end
  end

  def update
    if current_warga
      if @warga.update_attributes(update_params)
        render json: {
          status: 'success',
          message: 'berhasil update warga',
          result: current_warga
        },
        status: :created
      else
        render json: {
          status: 'error',
          message: @warga.errors.full_messages
        },
        status: :unprocessable_entity
      end
    else
      render json: {
        status: 'error',
        message: 'Token Invalid'
      },
      status: :unprocessable_entity
    end
  end

  private
  def update_params
  	params.permit(:email, :phone, :password, :personel_status)
  end

  def set_warga
    @warga = Warga.find(current_warga.id)
  end
end
