# frozen_string_literal: true

class WebController < ApplicationController
  include Knock::Authenticable
  include Response
  include ExceptionHandler
end
