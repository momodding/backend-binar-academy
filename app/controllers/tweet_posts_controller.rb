# frozen_string_literal: true

class TweetPostsController < ApplicationController
  before_action :set_tweet, only: %i[show update destroy]

  # GET /todos
  def index
    @tweet_posts = TweetPost.all
    # json_response(@tweet_posts)
    render json: { message: 'Ok', result: @tweet_posts }, status: :ok
  end

  # POST /todos
  def create
    @tweet_posts = TweetPost.create!(tweet_params)
    # json_response(@tweet_posts, :created)
    if @tweet_posts.save
      render json: { status: 'Succes',
                     message: 'Post Inserted', result: @tweet_posts }, status: :created # 201
    else
      render json: { status: 'Failed',
                     message: 'Failed inserting Data' }, status: :unprocessable_entity # status code
  end
  end

  # GET /todos/:id
  def show
    json_response(@tweet_posts)
  end

  # PUT /todos/:id
  def update
    @tweet_posts.update(tweet_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @tweet_posts.destroy
    head :no_content
  end

  private

  def tweet_params
    # whitelist params
    params.permit(:user_tweets_id, :tweets_id, :title, :description, :url, :img_url)
  end

  def set_tweet
    @tweet_posts = TweetPost.find(params[:id])
  end
end
