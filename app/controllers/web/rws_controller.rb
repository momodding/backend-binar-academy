# frozen_string_literal: true

class Web::RwsController < WebController
  before_action :authenticate_user # except untuk disable auth pada method
  before_action :set_dukuh, only: %i[show]
  before_action :set_rw, only: %i[update destroy]
  # GET /todos
  def index
    @rw = Rw.all

    render json: { message: 'Ok', result: @rw }, status: :ok
  end

  def create
    register = Rw.create(register_params)
    if register.save
      render json: {
        status: 'success',
        message: 'Rw Created',
        result: register
      },
             status: :created # status code 201
    else
      render json: {
        status: 'error',
        message: 'Failed!',
        result:
          # cara 1
          register.errors # menampilkan semua error
      },
             status: :unprocessable_entity # status code 422
    end
  end

  # GET /todos/:id
  def show # SHOW by DUKUH
    @rw = @dukuh.rws.all
    render json: {
      status: 'ok',
      result: @rw
    }, status: 200
  end

  # PUT /todos/:id
  def update
    @rw.update(register_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @rw.destroy
    head :no_content
  end

  private

  def register_params
    params.permit(:name, :dukuh_id)
  end

  def set_dukuh
    @dukuh = Dukuh.find(params[:id])
  end

  def set_rw
    @rw = Rw.find(params[:id])
  end
end
