# frozen_string_literal: true

class Web::DukuhsController < WebController
  before_action :authenticate_user # except untuk disable auth pada method
  before_action :set_dukuh, only: %i[show update destroy]
  before_action :set_desa

  # GET /todos
  def index
    @dukuh = Dukuh.where(desa_id: @desa.id).order(:name)
    render json: { message: 'Ok', result: @dukuh }, status: :ok
  end

  def create
    register = Dukuh.create(name: register_params[:name], desa_id: @desa.id)
    if register.save
      render json: {
        status: 'success',
        message: 'Dukuh Created',
        result: register
      }, status: :created # status code 201
    else
      render json: {
        status: 'error',
        message: 'Failed!',
        result: register.errors # menampilkan semua error
      }, status: :unprocessable_entity # status code 422
    end
  end

  # GET /todos/:id
  def show
    json_response(@dukuh)
  end

  def show_current_desa
    @user = User.find(current_user.id)
    @dukuh = @user.desa.dukuhs.all
    render json: {
      status: 'ok',
      result: @dukuh
    }, status: 200
  end

  # PUT /todos/:id
  def update
    @dukuh.update(register_params)
    head :no_content
  end

  # DELETE /todos/:id
  def destroy
    @dukuh.destroy
    head :no_content
  end

  private

  def register_params
    params.permit(:name)
  end

  def set_dukuh
    @dukuh = Dukuh.find(params[:id])
  end

  def set_desa
    # @id = current_user.id
    # @desa = Desa.find_by(user_id: @id)

    @desa = current_user.desa
  end
end
