# frozen_string_literal: true

class Web::HelpsController < WebController
  before_action :authenticate_user, except: :show_by_desa # untuk disable auth pada method
  before_action :set_desa, only: %i[show_by_desa delete_all_by_desa]
  before_action :set_help, only: %i[delete_by_id edit_by_id show_by_id]

  # POST Help
  def create
    @help = Help.create(help_params)

    if @help.save
      render json: {
        status: 'success',
        message: :created,
        result: @help
      },
             status: :created
    else
      render json: {
        status: 'error',
        message: :unprocessable_entity,
        result: @help.errors
      },
             status: :unprocessable_entity
      end
  end

  # GET All Helps by Desa
  def show_by_desa
    @helps = Help.where(desa_id: @desa.id)
    render json: {
      status: 'success',
      message: :ok,
      result: @helps
    },
           status: :ok
  end

  # GET Help by Id
  def show_by_id
    render json: {
      status: 'success',
      message: :ok,
      result: @help
    },
           status: :ok
  end

  # DELETE Help By Id
  def delete_by_id
    @help.destroy
    render json: {
      status: 'success',
      message: :ok,
      result: nil
    },
           status: :ok
  end

  # DELETE All Helps By Desa
  def delete_all_by_desa
    @helps = Help.where(desa_id: @desa.id)
    @helps.destroy_all
    render json: {
      status: 'success',
      message: :ok,
      result: nil
    },
           status: :ok
  end

  # EDIT Help by Id
  def edit_by_id
    @help.update(help_params)
    render json: {
      status: 'success',
      message: :created,
      result: @help
    },
           status: :created
  end

  def help_params
    params.permit(:title, :desa_id)
  end

  def set_desa
    @desa = Desa.find(params[:id])
  end

  def set_help
    @help = Help.find(params[:id])
  end
end
