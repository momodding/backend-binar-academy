# frozen_string_literal: true

class Web::UsersController < WebController
  # before_action :authenticate_user, except: :check_token, :reset_password #except untuk disable auth pada method
  before_action :set_user, only: %i[show update destroy]

  def check_token
    if current_user
      # HardWorker.perform_async(current_user.email)
      # PostingWorker.perform_async
      render json: {
        status: 'success',
        message: 'Token Valid',
        data: current_user
      },
             status: :ok
    else
      render json: {
        status: 'error',
        message: 'Token Invalid'
      },
             status: :unprocessable_entity
    end
  end

  def create
    user = User.create(allow_params)
    # user.password_confirmation = user.password
    if user.save
      render json: {
        status: 'success',
        message: 'User dibuat',
        result: user
      },
             status: :created # status code 201
    else
      render json: {
        status: 'error',
        message: 'User gagal dibuat',
        result:
          # cara 1
          user.errors # menampilkan semua error

        # #cara 2
        # {
        #   name: user.errors[:name],
        #   email: user.errors[:email],
        #   password: user.errors[:password]
        # }
      },
             status: :unprocessable_entity # status code 422
    end
  end

  def reset_password
    params.permit(:email)
    @user = User.find(params[:email])
  end

  private

  def allow_params
    params.permit(:name, :email, :password)
  end

  def set_user
    @user = User.find(params[:id])
  end
end
