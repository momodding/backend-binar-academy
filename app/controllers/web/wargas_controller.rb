# frozen_string_literal: true

class Web::WargasController < WebController
  before_action :authenticate_user
  before_action :set_warga, only: %i[show update destroy reset_password]

  def index
    @wargas = Warga.all
    render json: {
      status: 'success',
      message: :ok,
      result: @wargas
    }, status: :ok
  end

  def show_by_desa
    @desa = Desa.find_by(user_id: current_user.id)
    @wargas = Warga.where(desa_id: @desa.id)
    render json: {
      status: 'success',
      message: :ok,
      result: @wargas
    }, status: :ok
    end

  def show
    render json: {
      status: 'success',
      message: :ok,
      result: @warga
    }, status: :ok
  end

  def create
    @warga = Warga.new(warga_params)
    # @warga.password_digest = SecureRandom.hex(3)
    if @warga.save
      # Passwordmail.new_password(@warga.email, @warga.name, @warga.password).deliver_now
      UserMailer.welcome_email(@warga.email).deliver_now

      render json: {
        status: 'success',
        message: 'create warga success',
        result: @warga
      }, status: :created
    else
      render json: {
        status: 'error',
        message: 'gagal buat warga',
        result: @warga.errors
      }, status: :unprocessable_entity
    end
  end

  def update
    @warga.update(warga_params)
    render json: {
      status: 'success',
      message: :created,
      result: @warga
    }, status: :created
  end

  def reset_password
    if @warga.update_attributes(resetpassword_params)
      ResetPasswordMailer.reset_email(@warga, resetpassword_params['password']).deliver_now
      render json: {
        status: 'success',
        message: :created,
        result: @warga
      }, status: :created
    else
      render json: {
        status: 'error',
        message: 'gagal buat warga',
        result: @warga.errors
      }, status: :unprocessable_entity
  end
  end

  def destroy
    @warga.destroy
    render json: {
      status: 'success',
      message: :ok,
      result: nil
    }, status: :ok
  end

  private

  def set_warga
    @warga = Warga.find(params[:id])
  end

  def warga_params
    params.permit(:name, :nik, :email, :role, :rt_id, :rw_id, :dukuh_id, :phone, :password, :desa_id, :alamat, :personal_status)
  end

  def resetpassword_params
    params.permit(:password)
  end
end
