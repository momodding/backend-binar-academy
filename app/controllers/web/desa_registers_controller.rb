# frozen_string_literal: true

class Web::DesaRegistersController < WebController
  before_action :authenticate_user, except: :create # except untuk disable auth pada method

  def create
    register = DesaRegister.create(register_params)
    if register.save
      UserMailer.welcome_email(register.email).deliver_now
      render json: {
        status: 'success',
        message: 'Success! Your Registration Under Review!',
        result: register
      },
             status: :created # status code 201
    else
      render json: {
        status: 'error',
        message: 'Registration Failed!',
        result:
          {
            name: register.errors[:name],
            email: register.errors[:email],
            province: register.errors[:province],
            district: register.errors[:district],
            subdistrict: register.errors[:subdistrict],
            file: register.errors[:file]
          }
      },
             status: :unprocessable_entity # status code 422
    end
  end

  private

  def register_params
    params.permit(:name, :email, :province, :district, :subdistrict, :file)
  end
end
