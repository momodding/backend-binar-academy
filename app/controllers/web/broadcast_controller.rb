# frozen_string_literal: true

class Web::BroadcastController < WebController
  before_action :authenticate_user
  def create
    user = User.find(params[:id])
    tweet = user.broadcast_posts.create(tweet_params)
    if tweet.save
      render json: { status: 'Succes',
                     message: 'Tweet berhasil dipost', result: tweet }, status: :created # 201
    else
      render json: { status: 'Failed',
                     message: 'Tweet Gagal dipost' }, status: :unprocessable_entity # status code
    end
  end

  # def show
  #   user = User.find(params[:id])
  #   tweet = user.broadcast_posts.find(params[:tweets_id])
  #   render json: { tweet: tweet.content, user: user.name }, status: :ok
  # end

  def showall
    user = User.find(params[:id])
    tweet = user.broadcast_posts.all
    render json: { tweet: tweet, user: user.name }, status: :ok
  end

  private

  def tweet_params
    params.permit(:content) # whiteListing parameter
  end
end
