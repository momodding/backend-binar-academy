# frozen_string_literal: true

class Web::UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
