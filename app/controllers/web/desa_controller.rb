# frozen_string_literal: true

class Web::DesaController < WebController
  before_action :authenticate_user
  before_action :set_desa, only: %i[show update destroy]

  # GET /api/v1/desa
  def index
    @desa = Desa.all
    render json: {
      status: 'success',
      message: :ok,
      result: @desa
    },
    status: :ok
  end

  # GET /api/v1/desa/:id
  def show
    render json: {
      status: 'success',
      message: :ok,
      result: @desa
    },
    status: :ok
  end

  # POST /api/v1/desa
  def create
    @desa = Desa.create(desa_params)
    if @desa.save
      render json: {
        status: 'success',
        message: :ok,
        result: @desa
      },
      status: :ok
    else
      render json: {
        status: 'error',
        message: :unprocessable_entity,
        result: @desa.errors
      },
      status: :unprocessable_entity
    end
  end

  # PUT /api/v1/desa/:id
  def update
    @desa.update(desa_params)
    render json: {
      status: 'success',
      message: :created,
      result: @desa
    },
    status: :created
  end

  # DELETE /api/v1/desa/:id
  def destroy
    @desa.destroy
    render json: {
      status: 'success',
      message: :ok,
      result: nil
    },
    status: :ok
  end

  private

  def desa_params
    params.permit(:name, :province, :district, :subdistrict, :user_id)
  end

  def set_desa
    @desa = Desa.find(params[:id])
  end
end
