# frozen_string_literal: true

class Web::RtController < WebController
  before_action :authenticate_user
  before_action :set_rt, only: %i[update destroy]
  before_action :set_rw, only: %i[show]

  # GET /web/rt
  def index
    @rts = Rt.all
    render json: {
      status: 'success',
      message: :ok,
      result: @rts
    },
           status: :ok
  end

  # GET /web/rt/:id
  def show
    @rt = @rw.rts.all
    render json: {
      status: 'success',
      message: :ok,
      result: @rt
    },
           status: :ok
  end

  # POST /web/rt
  def create
    @rt = Rt.create(rt_params)
    if @rt.save
      render json: {
        status: 'success',
        message: :created,
        result: @rt
      },
             status: :created
    else
      render json: {
        status: 'error',
        message: :unprocessable_entity,
        result: @rt.errors
      },
             status: :unprocessable_entity
    end
  end

  # PUT /web/rt/:id
  def update
    @rt.update(rt_params)
    render json: {
      status: 'success',
      message: :created,
      result: @rt
    },
           status: :created
  end

  # DELETE /web/rt/:id
  def destroy
    @rt.destroy
    render json: {
      status: 'success',
      message: :ok,
      result: nil
    },
           status: :ok
  end

  private

  def rt_params
    params.permit(:name, :rw_id)
  end

  def set_rt
    @rt = Rt.find(params[:id])
  end

  def set_rw
    @rw = Rw.find(params[:id])
  end
end
