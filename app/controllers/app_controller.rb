# frozen_string_literal: true

class AppController < ApplicationController
  include Knock::Authenticable
end
