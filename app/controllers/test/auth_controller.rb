# frozen_string_literal: true

class Test::AuthController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
