# frozen_string_literal: true

class Test::HelloController < WebController
  def hello
    render json: {
      message: 'oke'
    }, status: :ok
  end
end
