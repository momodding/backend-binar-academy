# frozen_string_literal: true

class Test::UsersAuthController < WebController
  # secured controller (untuk cek auth)
  before_action :authenticate_user, except: :check_token # except untuk disable auth pada method

  def create
    user = User.create(allow_params)
    # user.password_confirmation = user.password
    if user.save
      render json: {
        status: 'success',
        message: 'User dibuat',
        result: user
      },
             status: :created # status code 201
    else
      render json: {
        status: 'error',
        message: 'User gagal dibuat',
        result:
          # cara 1
          user.errors # menampilkan semua error

        # #cara 2
        # {
        #   name: user.errors[:name],
        #   email: user.errors[:email],
        #   password: user.errors[:password]
        # }
      },
             status: :unprocessable_entity # status code 422
    end
  end

  private

  def allow_params
    params.permit(:name, :email, :password)
  end
end
