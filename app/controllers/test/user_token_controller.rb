# frozen_string_literal: true

class Test::UserTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
