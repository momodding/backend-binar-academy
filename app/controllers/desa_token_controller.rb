# frozen_string_literal: true

class DesaTokenController < Knock::AuthTokenController
  skip_before_action :verify_authenticity_token
end
