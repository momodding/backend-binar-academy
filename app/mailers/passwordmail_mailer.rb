# frozen_string_literal: true

class PasswordmailMailer < ApplicationMailer
  def new_password(email, name, password)
    @name = name
    @password = password
    @email = email
    mail(to: @email, subject: 'Sadarsiaga New Password')
  end
end
