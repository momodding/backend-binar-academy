# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def welcome_email(email)
    mail(to: email, subject: 'Sadar Siaga')
  end

  def reset_email(user, _password)
    @user = user
    mail(to: @user.email, subject: 'Sadar Siaga Reset Password')
  end
end
