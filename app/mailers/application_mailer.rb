# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'time.binar@gmail.com'
  layout 'mailer'
end
