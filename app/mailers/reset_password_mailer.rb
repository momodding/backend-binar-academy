# frozen_string_literal: true

class ResetPasswordMailer < ApplicationMailer
  def reset_email(user, _password)
    @user = user
    @password = _password
    mail(to: @user.email, subject: 'Sadar Siaga Reset Password')
  end
end
