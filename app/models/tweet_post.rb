# frozen_string_literal: true

class TweetPost < ApplicationRecord
  validates_presence_of :user_tweets_id, :tweets_id, :title, :description, :url, :img_url
  validates_uniqueness_of :tweets_id
end
