# frozen_string_literal: true

class Rw < ApplicationRecord
  has_many :wargas, foreign_key: 'rw_id'
  has_many :rts, foreign_key: 'rw_id'
  belongs_to :dukuh, foreign_key: 'dukuh_id'

  validates_presence_of :name
end
