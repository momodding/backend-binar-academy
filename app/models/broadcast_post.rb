# frozen_string_literal: true

class BroadcastPost < ApplicationRecord
  belongs_to :user
  validates_presence_of :content
end
