# frozen_string_literal: true

class Desa < ApplicationRecord
  has_many :dukuhs, foreign_key: 'desa_id'
  has_many :wargas, foreign_key: 'desa_id'
  belongs_to :user, foreign_key: 'user_id'

  validates_presence_of :name, :province, :district, :subdistrict, :user_id
end
