# frozen_string_literal: true

class Dukuh < ApplicationRecord
  has_many :wargas, foreign_key: 'dukuh_id'
  has_many :rws, foreign_key: 'dukuh_id'
  belongs_to :desa, foreign_key: 'desa_id'

  validates_presence_of :name
end
