# frozen_string_literal: true

class DesaRegister < ApplicationRecord
  EMAIL_REGEX = /\A[^@\s]+@[^@\s]+\z/.freeze # validasi format email

  # validasi input
  validates :name, presence: true
  validates :email, presence: true, uniqueness: true, format: { with: EMAIL_REGEX }
  validates :province, presence: true
  validates :district, presence: true
  validates :subdistrict, presence: true
  # validates :file
end
