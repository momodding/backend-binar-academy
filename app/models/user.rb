# frozen_string_literal: true

class User < ApplicationRecord
  # relation
  has_one :desa, foreign_key: 'user_id'
  has_many :broadcast_posts
  # tipe data
  enum role: { admin: 'admin', desa: 'desa' }
  enum key_type: { activation: 'activation', forgot: 'forgot' }

  # hash password
  has_secure_password

  # validasi
  EMAIL_REGEX = /\A[^@\s]+@[^@\s]+\z/.freeze # validasi format email

  # validates :email, presence: true, uniqueness: true, format:{with: EMAIL_REGEX} #cara dari binar
  validates :name, presence: true # presence: true -> harus diisi
  validates :email, presence: true, uniqueness: true, # uniqueness -> tidak boleh sama
                    format: {
                      with: EMAIL_REGEX
                    }

  # custom token
  # def self.from_token_request request
  #   email = request.params["auth"] && request.params["auth"]["email"]
  #   self.includes(:desa).find_by(email: email)
  # end
  def to_token_payload
    # Returns the payload as a hash
    payload = {
      sub: id,
      # desa_id:  self.desa.id
    }
  end
end
