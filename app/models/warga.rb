# frozen_string_literal: true

class Warga < ApplicationRecord
  before_save :default_values

  # database
  has_secure_password
  enum role: { desa: 'desa', dukuh: 'dukuh', rw: 'rw', rt: 'rt', warga: 'warga' }
  belongs_to :rt, foreign_key: 'rt_id'
  belongs_to :rw, foreign_key: 'rw_id'
  belongs_to :dukuh, foreign_key: 'dukuh_id'
  # belongs_to :desa, foreign_key: "desa_id"

  # validasi
  EMAIL_REGEX = /\A[^@\s]+@[^@\s]+\z/.freeze # validasi format email

  validates_presence_of :name, :nik, :email, :password, :role, :dukuh_id, :rw_id, :rt_id
  validates_uniqueness_of :nik, :email
  validates :email, format: { with: EMAIL_REGEX }

  def default_values
    self.is_active = true if is_active.nil?
  end

  # custom get token knock
  def self.from_token_request(request)
    @username = request.params['auth'] && request.params['auth']['username']
    # @result = self.find_by(email: @username)
    find_by("email = '#{@username}' OR nik = '#{@username}' OR phone = '#{@username}'")
  end
end
