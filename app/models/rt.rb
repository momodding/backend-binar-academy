# frozen_string_literal: true

class Rt < ApplicationRecord
  has_many :wargas, foreign_key: 'rt_id'
  belongs_to :rw, foreign_key: 'rw_id'

  validates_presence_of :name
end
