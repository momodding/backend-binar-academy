# frozen_string_literal: true

class Help < ApplicationRecord
  belongs_to :desa, foreign_key: 'desa_id'

  validates_presence_of :title, :desa_id
end
