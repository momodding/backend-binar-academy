# frozen_string_literal: true

class Rw < ApplicationRecord
  has_many :wargas, dependent: :destroy
  has_many :rts, through: :wargas
  has_many :desas, through: :wargas

  validates_presence_of :name
end
