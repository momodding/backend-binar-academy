# frozen_string_literal: true

class User < ApplicationRecord
  # db relation
  # has_many :tweets
  has_one :warga

  # hash password
  has_secure_password

  # validasi
  EMAIL_REGEX = /\A[^@\s]+@[^@\s]+\z/.freeze # validasi format email

  # validates :email, presence: true, uniqueness: true, format:{with: EMAIL_REGEX} #cara dari binar
  # validates :name, presence: true #presence: true -> harus diisi
  validates :email, presence: true, uniqueness: true, # uniqueness -> tidak boleh sama
                    format: {
                      with: EMAIL_REGEX
                    }
end
