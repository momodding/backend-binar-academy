# frozen_string_literal: true

class Warga < ApplicationRecord
  has_one :app_user, dependent: :destroy
  belongs_to :rw
  belongs_to :rt
  belongs_to :desa
  # belongs_to :user
  validates_presence_of :name, :rt_id, :rw_id, :desa_id
end
