# frozen_string_literal: true

class AppUser < ApplicationRecord
  belongs_to :warga

  # hash password
  has_secure_password

  # validasi
  EMAIL_REGEX = /\A[^@\s]+@[^@\s]+\z/.freeze # validasi format email

  validates_presence_of :name, :password, :role
  validates :email, presence: true, uniqueness: true, # uniqueness -> tidak boleh sama
                    format: {
                      with: EMAIL_REGEX
                    }
end
