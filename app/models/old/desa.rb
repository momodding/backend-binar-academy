# frozen_string_literal: true

class Desa < ApplicationRecord
  has_many :wargas, dependent: :destroy
  has_many :rts, through: :wargas
  has_many :rws, through: :wargas

  validates_presence_of :name
end
