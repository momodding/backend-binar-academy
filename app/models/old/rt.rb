# frozen_string_literal: true

class Rt < ApplicationRecord
  has_many :wargas, dependent: :destroy
  has_many :desas, through: :wargas
  has_many :rws, through: :wargas

  validates_presence_of :name
end
