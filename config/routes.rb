# frozen_string_literal: true

Rails.application.routes.draw do
  # resources :wargas
  resources :tweet_posts

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :app do
    post 'auth/login' => 'warga_token#create'
    get 'auth/checktoken' => 'wargas#check_token'
    get 'profile/info' => 'wargas#profile'
    put 'profile/update' => 'wargas#update'
  end

  namespace :web do
    # auth
    post 'auth/login' => 'user_token#create'
    post 'auth/signup' => 'desa_registers#create'
    post 'auth/admin_signup' => 'users#create'
    post 'auth/reset_password' => 'users#reset_password'
    post 'auth/checktoken' => 'users#check_token'

    # dukuh
    resources :dukuhs

    # dukuh new
    get 'dukuh/all/' => 'dukuhs#index'
    post 'dukuh/create' => 'dukuhs#create'
    get 'dukuh/show/all' => 'dukuhs#show_current_desa'
    # broadcast
    post 'broadcast/create/:id' => 'broadcast#create'
    get 'broadcast/:id' => 'broadcast#showall'

    # desa
    get 'desa' => 'desa#index'
    get 'desa/:id' => 'desa#show'
    post 'desa' => 'desa#create'
    put 'desa/:id' => 'desa#update'
    delete 'desa/:id' => 'desa#destroy'

    # RW
    resources :rws

    # RR
    resources :rt

    # warga
    resources :wargas
    get 'warga/desa' => 'wargas#show_by_desa'
    post 'warga/create' => 'wargas#create'
    patch 'warga/reset_password/:id' => 'wargas#reset_password'

    # help
    get 'helps/all/:id' => 'helps#show_by_desa'
    get 'helps/:id' => 'helps#show_by_id'
    post 'helps' => 'helps#create'
    delete 'helps/:id' => 'helps#delete_by_id'
    delete 'helps/all/:id' => 'helps#delete_all_by_desa'
    put 'helps/:id' => 'helps#edit_by_id'
  end

  # test
  namespace :test do
    get 'hello' => 'hello#hello'
    post 'user/create' => 'users#create'
    post 'user-auth/create' => 'users_auth#create'
    post 'auth/login' => 'auth#create'
    post 'user_token' => 'user_token#create'
  end
end
